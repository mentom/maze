import random
import djikstr as djikstr
import itertools
import utils as utils

def main():

    print("******* MAZE BEST PATH FINDER ********")
    realMaze, m, n = utils.createRealMaze()
    realMaze.print_maze_optimized()

    # Creating a Tuple array with edges and nodes
    edges, s, v = utils.defineEdges(m, n, realMaze)
    print("******** DJIKSTRA: shortest path algorithm ************")
    result = djikstr.dijkstra(edges, s, (v))
    steps = utils.flatStepsMultiTuple(result)
    utils.printShortestPath(steps, realMaze)
    print("******** THAT'S ALL ************")

     

if __name__ == "__main__":
    main()