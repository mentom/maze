import random

class Maze(object):
    m = 0
    n = 0
    matrix = ""
    empty_matrix = ""

    def __init__(self, m, n):
        self.m = m
        self.n = n
        self.matrix = [[random.uniform(0, 1) for x in range(m)] for y in range(n)]
        self.empty_matrix = [['#' for x in range(m)] for y in range(n)]

    def print_maze(self):
        for row in self.matrix:
            for val in row:
                print "%.2f" % val,
            print

    def print_maze_optimized(self):
        print('\n'.join(['\t'.join(["%.2f" % item for item in row]) 
      for row in self.matrix]))

    def print_empty_maze_optimized(self):
        print('\n'.join(['\t'.join([item for item in row]) 
      for row in self.empty_matrix]))

    def create_edges(self):
        edges = []
        i = 0
        j = 0
        for row in self.matrix:
            for val in row:
                val = (str(i),str(j))
                string_val = ",".join(val)

                if i+1 < self.n:
                    valR = (str(i+1),str(j))
                    string_valR = ",".join(valR)
                    edgeR = (string_val, string_valR, self.matrix[i+1][j])
                    edges.append(edgeR)
                if i-1 >= 0:
                    valL = (str(i-1),str(j))
                    string_valL = ",".join(valL)
                    edgeL = (string_val, string_valL, self.matrix[i-1][j])
                    edges.append(edgeL)
                if j+1 < self.m:
                    valB = (str(i),str(j+1))
                    string_valB = ",".join(valB)
                    edgeB = (string_val, string_valB, self.matrix[i][j+1])
                    edges.append(edgeB)
                if j-1 >= 0:
                    valT = (str(i),str(j-1))
                    string_valT = ",".join(valT)
                    edgeT = (string_val, string_valT, self.matrix[i][j-1])
                    edges.append(edgeT)
                j = j + 1
            j = 0
            i = i + 1
        return edges

def make_maze(m, n):
    return Maze(m, n)

