from collections import defaultdict
from heapq import *

#https://gist.github.com/kachayev/5990802

def dijkstra(edges, f, t):
    g = defaultdict(list)
    for l,r,c in edges:
        g[l].append((c,r))  #Per ogni nodo nel dictionary 
                            #ho nodi raggiungibili ed il costo per raggiungerli in base agli archi

    q, seen, mins = [(0,f,())], set(), {f: 0}
    while q:
        (cost,v1,path) = heappop(q) #Pop dei nodi dall'heap (FIFO), per ogni nodo calcola il costo
        if v1 not in seen:
            seen.add(v1)
            path = (v1, path)
            if v1 == t: return (cost, path)
    
            for c, v2 in g.get(v1, ()): # Esamina ogni nodo raggiungibile da v1
                if v2 in seen: continue
                prev = mins.get(v2, None)
                next = cost + c
                if prev is None or next < prev:
                    mins[v2] = next
                    heappush(q, (next, v2, path))

    return float("inf")

