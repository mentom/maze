import maze

def flatStepsMultiTuple(result):
    flatten = lambda lst: reduce(lambda l, i: l + flatten(i) if isinstance(i, (list, tuple)) else l + [i], lst, [])  
    steps = flatten(result[1])
    steps = list(reversed(steps))
    print('Steps: ' + str(steps))
    return steps

def defineEdges(m, n, realMaze):
    s = ('0,0')
    v = ('' + str(m-1) + ',' + str(n-1)+'')

    print("Source: " + str(s) + "; Destination: " + str(v))

    edges = realMaze.create_edges()
    return edges, s, v

def createRealMaze():
#    m = input("Enter rows: ")
#    n = input("Enter cols: ")
    m = inputNumber("Enter rows: ")
    n = inputNumber("Enter cols: ")


    realMaze = maze.make_maze(n,m)

    matrixDescription = "%sX%s weights matrix."%(n,m)

    print(matrixDescription)
    return realMaze, m, n

def printShortestPath(steps, realMaze):
    for vertex in steps:
        v = str(vertex).split(",")
        realMaze.empty_matrix[int(v[0])][int(v[1])] = 'O'

    print("Shortest path")
    realMaze.print_empty_maze_optimized()

def inputNumber(message):
  while True:
    try:
       userInput = int(raw_input(message))       
    except ValueError:
       print("Not an integer! Try again.")
       continue
    else:
       return userInput 
       break 
